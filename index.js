let trainer={
    // Properties
    name: "Ash Ketchum",
    age: 10,
    pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
    friends: {
        Hoenn: ['May', 'Max'],
        Kanto: ['Brock','Misty']
    },


    talk: function(){
        console.log("Pikachu! I choose you!");
    }
}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log(trainer.pokemon);
trainer.talk();


function Pokemon(name, level){
    // Properties
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);

        target.health -= this.attack
        console.log(target.name + " health is now reduced to " + target.health);
        console.log(target);

        if(target.health <=0){
            target.faint();
            console.log(target);
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted.");
    }
}



let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);